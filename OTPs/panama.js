module.exports = {
  token:
    "eyJhbGciOiJSUzI1NiIsImprdSI6Imh0dHBzOi8vbWJhYXMuZGVzYS5jby5kYXZpdmllbmRhLmNvbS9hdXRoL3YxL2tleXN0b3JlLy53ZWxsLWtub3duL2p3a3MuanNvbiIsImtpZCI6IjE1Njc3MzYzNzAifQ.eyJhdWQiOiJEQVY6Q0xPVUQ6QVVUSCIsImV4cCI6MTU2ODY0OTM5NCwiaWF0IjoxNTY4NjQ4Nzk0LCJpc3MiOiJEQVY6Q0xPVUQ6QVVUSCIsInN1YiI6IjI2NmE1NzEwLWQ4OTktMTFlOS05NjFjLTQ3Nzc2MTFlOTA5NSIsInVzZSI6ImEiLCJwcm9kdWN0IjoiTU5VSU5HX1NWXzQ4IiwianRpIjoiMjkxZWExYTAtZDg5OS0xMWU5LTkzYzktMWQ1OWYzY2Q2OTczIn0.ZkeBZi1bELE7hDRIndfsRlPIPa14Hm_zxxiZ1MkT7EOqspbQucDaUIA594YAmV4f-zHDWqRRzxwrx7CB-2KaxNJaebwLrDlj3cIksnLZGaZ9n9wiQdmHmoZbuFTNhQimBQvKRZCMuemkPYMHrXkIyu967P23gQ-1U5iD-mSX-Teqg37f71nOYDX7H_FNqc8W9SRWfuoT0ZakyoF20DdjaIt8kkI3mCmrC7k5VEFYHDgj-6wDgwAE3vPjvwD2Y2QHPBnGiLnk6Nnu91LnGPweTX908uQG81tgi97uxFzl3jY1ciGf1E4pTEqmhygfZm5jMbxcbF12F0xY9rC-fhiT1w",
  workflow: {
    APPBOOT: "ING001",
    CUE: "CUE010",
    // CUE010: "ADN001",
    // ADN001: "CUE180",
    // Clave virtual
    // CUE010: 'CUE200',
    // CUE190: 'CUE200',
    // CUE170: 'CUE180',

    // Biometria
    CUE010: 'CUE030',
    CUE030: 'CUE040',
    CUE040: 'CUE050',


    // CUE180: 'CUE010',
    // CUE010: "ADN001",
    // CUE010: "CUE030",
    CUE030: "CUE040",
    CUE040: "CUE050",
    BIO003: "ADN001",
    // CUE070: "ADN001", // de ben001 - a vin001
    CUE070: "CUE080", // de vin001 - vin002
    CUE080: "CUE090", // de vin002 - vin003
    CUE090: "CUE100", // de vin003 - vin004
    CUE100: "CUE110", // de vin004 - vin005
    CUE110: "CUE120", // de vin005 - v  in006
    CUE120: "CUE130", // de vin006 - vin007
    CUE130: "CUE140", // de vin007 - vin008
    CUE140: "CUE150", // de vin008 - vin009
    CUE150: "CUE160",
    CTNM010: "VIN001",
    ING001: "PCL001",
    PCL001: "TPC001",
    TPC001: "BEN001",
    BEN001: "VIN001",
    VIN001: "VIN002",
    VIN002: "VIN003",
    VIN003: "VIN004",
    VIN004: "VIN005",
    VIN005: "VIN006",
    VIN006: "VIN007",
    VIN007: "VIN008",
    VIN008: "VIN009",
    // VIN005: "OTP001",
    OTP001: "OTP001"
  },
  CUE010: {
    status: 1,
    payload: {
      canal: "37",
      esCliente: true,
      idModulo: "CTANOM",
      modulo: "CTANOM",
      lenguaje: "ES",
      pais: "CO"
    }
  },
  CUE030: {
    status: 1,
    payload: {
      canal: "37",
      esCliente: true,
      idModulo: "CTANOM",
      modulo: "CTANOM",
      lenguaje: "ES",
      pais: "CO"
    }
  },
  CUE040: {
    status: 1,
    payload: {
      canal: "37",
      esCliente: false,
      idModulo: "CTANOM",
      modulo: "CTANOM",
      lenguaje: "ES",
      pais: "CO"
    }
  },
  CUE050: {
    status: 1,
    payload: {
      canal: "37",
      esCliente: false,
      idModulo: "CTANOM",
      modulo: "CTANOM",
      lenguaje: "ES",
      pais: "CO"
    }
  },
  ING001: {
    status: 1,
    payload: {
      aliado: "ML",
      canal: "37",
      esCliente: false,
      idModulo: "cam",
      lenguaje: "es",
      pais: "PA"
    }
  },
  PCL001: {
    status: 1,
    payload: {
      documento: {
        tipo: "E",
        numero: "8-888-888"
      }
    }
  },
  BEN001: {
    status: 1,
    payload: {
      titulo:
        "Conviértase en Cliente Davivienda en menos tiempo abriendo su Cuenta de Ahorros Móvil y acceda a los siguientes beneficios:"
    }
  },
  TPC001: {
    status: 1,
    payload: {}
  },
  BIO001: {
    status: 1,
    payload: {
      identificadorPaso: '1',
      llaveEnrolamiento: 'llave 1',
      canal: "37",
      esCliente: false,
      idModulo: "CTANOM",
      modulo: "CTANOM",
      lenguaje: "ES",
      pais: "CO"
    }
  },
  BIO002: {
    status: 1,
    payload: {
      identificadorPaso: '2',
      llaveEnrolamiento: 'llave 2',
      canal: "37",
      esCliente: false,
      idModulo: "CTANOM",
      modulo: "CTANOM",
      lenguaje: "ES",
      pais: "CO"
    }
  },
  BIO003: {
    status: 1,
    payload: {
      identificadorPaso: '3',
      llaveEnrolamiento: 'llave 3',
      canal: "37",
      esCliente: false,
      idModulo: "CTANOM",
      modulo: "CTANOM",
      lenguaje: "ES",
      pais: "CO"
    }
  },
  BIO004: {
    status: 1,
    payload: {}
  },
  BIO005: {
    status: 1,
    payload: {}
  },
  BIO006: {
    status: 1,
    payload: {
      entidades: [
        {
          id: "0",
          nombre: "Ciudad de Panama"
        }
      ],
      operadores: [
        {
          id: "0",
          nombre: "Claro"
        }
      ]
    }
  },
  CUE070: {
    status: 1,
    payload: {
      nombre: {
        primerNombre: "sebastian",
        segundoNombre: "",
        primerApellido: "Abella",
        segundoApellido: "Rocha"
      },
      documento: {
        numero: "1223456799",
        tipo: "Cedula de ciudadania"
      },
      fechaNacimiento: "27/08/1999",
      genero: "masculino"
    }
  },
  CUE080: {
    status: 1,
    payload: {
      nombre: {
        primerNombre: "sebastian 2",
        segundoNombre: "",
        primerApellido: "Abella",
        segundoApellido: "Rocha"
      }
    }
  },
  CUE090: {
    status: 1,
    payload: {
      nombre: {
        primerNombre: "sebastian 3",
        segundoNombre: "",
        primerApellido: "Abella",
        segundoApellido: "Rocha"
      }
    }
  },
  CUE100: {
    status: 1,
    payload: {
      nombre: {
        primerNombre: "sebastian 4",
        segundoNombre: "",
        primerApellido: "Abella",
        segundoApellido: "Rocha"
      }
    }
  },
  CUE110: {
    status: 1,
    payload: {
      nombre: {
        primerNombre: "sebastian 5",
        segundoNombre: "",
        primerApellido: "Abella",
        segundoApellido: "Rocha"
      },
      actividadLaboral: "ama de casa",
      tipoOperaciones: [
        {
          id: "01",
          nombre: "Tipo Operación PA 1"
        }
      ]
    }
  },
  CUE120: {
    status: 1,
    payload: {
      nombre: {
        primerNombre: "sebastian 6",
        segundoNombre: "",
        primerApellido: "Abella",
        segundoApellido: "Rocha"
      },
      tipoIdentificaciones: [
        {
          id: "01",
          nombre: "DNI"
        }
      ]
    }
  },
  CUE130: {
    status: 1,
    payload: {
      nombre: {
        primerNombre: "sebastian 7",
        segundoNombre: "",
        primerApellido: "Abella",
        segundoApellido: "Rocha"
      }
    }
  },
  CUE140: {
    status: 1,
    payload: {
      nombre: {
        primerNombre: "sebastian 8",
        segundoNombre: "",
        primerApellido: "Abella",
        segundoApellido: "Rocha"
      }
    }
  },
  CUE150: {
    status: 1,
    payload: {
      nombre: {
        primerNombre: "sebastian 9",
        segundoNombre: "",
        primerApellido: "Abella",
        segundoApellido: "Rocha"
      },
      declaracionRecursos: "true"
    }
  },
  CUE160: {
    status: 1,
    payload: {
      nombre: {
        primerNombre: "sebastian sol",
        segundoNombre: "",
        primerApellido: "Abella",
        segundoApellido: "Rocha"
      },
      declaracionRecursos: "true"
    }
  },
  ACU001: {
    status: 1,
    payload: {}
  },
  AUT001: {
    status: 1,
    payload: {}
  },
  AUT002: {
    status: 1,
    payload: {}
  },
  ACU002: {
    status: 1,
    payload: {}
  },
  OTP001: {
    status: 1,
    payload: {}
  },
  ACU003: {
    status: 1,
    payload: {}
  },
  CUE170: {
    status: 1,
    payload: {
      esCliente: false,
      montoAprobado: 5000000
    }
  },
  CUE180_: {
    status: 6,
    payload: {
      message: 'catCUENTA_MSG_SVO_001',
      proteccionTarjeta: [
        {"valorCobertura": "100000001", "primaMensual": "12312312"},
        {"valorCobertura": "100000002", "primaMensual": "12312312"},
      ]
    }
  },
  CUE180: {
    status: 6,
    payload: {
      "message":"catCUENTA_MSG_SVO_001",
      "seguroVida": {
        minimo: "12000000",
        maximo: "60000000",
        planes: [
          {
            "maximo": "12000000",
            "minimo": "12000000",
            "primaAnual": "78000",
            "primaMensual": "6500"
          },
          {
            "maximo": "24000000",
            "minimo": "12000001",
            "primaAnual": "155000",
            "primaMensual": "12916.666666666666"
          },
          {
            "maximo": "36000000",
            "minimo": "24000001",
            "primaAnual": "233000",
            "primaMensual": "19416.666666666668"
          },
          {
            "maximo": "48000000",
            "minimo": "36000001",
            "primaAnual": "310000",
            "primaMensual": "25833.333333333332"
          },
          {
            "maximo": "60000000",
            "minimo": "48000001",
            "primaAnual": "388000",
            "primaMensual": "32333.333333333332"
          }
        ]
      },
      "proteccionTarjeta":[],
      "aliado":"LM",
      "canal":"37",
      "modulo":"CTANOM",
      "lenguaje":"ES",
      "pais":"CO"
    }
  },


  CUE210: {
    status: 1,
    payload: {
      // payload
      cuenta: {
        numeroCuenta: '54610216510531510',
        cuatroPorMil: true,
        asignacionClaveVirtual: true,
        asignacionTarjetaDebito: {
          estatus: true,
          esCiudadPrincipal: false,
          resultadoCreacionTarjetaDebito: true,
        },
        fechaCreacion: '15/01/2019',
        horaCreacion: '9:41am',
      },
      adelantoNomina: {
        estatus: true,
        numeroProducto: '957842',
        cupoAprobado: '1000000',
        numeroAprobacion: '8865412529554425',
        fechaCreacion: '15/01/2019',
        horaCreacion: '9:41am'
      },
      seguroVida: {
        estatus: true,
        numeroPoliza: '1035478',
        valorAsegurado: '24000000',
        valorPrima: '24000',
        fechaVigencia: '15/01/2019',
        numeroAprobacion: '48865412529554425',
        fechaCreacion: '15/01/2019',
        horaCreacion: '9:41am'
      },
      seguroProteccion: {
        estatus: true,
        numeroPoliza: '1035478',
        valorAsegurado: '24000000',
        valorPrima: '24000',
        fechaVigencia: '15/01/2019',
        numeroAprobacion: '48865412529554425',
        fechaCreacion: '15/01/2019',
        horaCreacion: '9:41am'
      }
    }
  },

  CUE200: {
    status: 1,
    payload: {
      tipoCuenta: 'Cuenta movil Nomina',
      cuatroPorMil: false,
      medioDepago: false,
      adelantoNomina: {
        estatus: true,
        cupoAprobado: '100000'
      },
      seguroDeVida: {
        estatus: true,
        valorAsegurado: '50000',
        primaMensual: '400000'
      },
      seguroProteccionTarjetas: {
        estatus: true,
        valorAsegurado: '800000',
        primaMensual: '120000'
      },
      montoTarifaCobro: '',
    esCliente:true
    }
  },

  ACV001: {
    payload: {}
  },

  CUE190: {
    status: 1 ,
    payload:{
      crypto: {
        credencial:`-----BEGIN PUBLIC KEY-----
        MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAsOPobkmaTkHXCdPKDmrv
        o0gtXRTdmHimLP+2QjQdM8JGA/9ErglUQ/cv8XfofS4ugcMMnItnS787bXk+PLGd
        HcQzB9R6oaTiCNpZmUypJe/Gy6l4drTjpThGJBd71LJSQy3qsILHViU+Zyi3uW0/
        gyHbKP40uuHUlTZl0EfG80GHjdxgswCtbQLk9ke8NYZlnoStjbGDIlz/nBG73yTG
        XEenX4Mhs0frFYKauD9Gj0CSYrMNkZ6NV++78OiHeiTvYtzaXVnfnhAc+QuA/dQY
        89uIquVe9ivTlHqrXAQLNivnjELJ4+fvJR5EwTrvUdL+lGozuKi3tPEgRITCeQ45
        eLjcHa64aTZaBBRTdeisg8s47+sqI2NHE9k3UYKy9zs0E1GGgW8TUKZdlZ4aLG7b
        tYzopJmcIXCAw8mhP+C81yG4qtAbHk/DK3vg2PSIS9G5Xdt17fb5D739+T/W4yh0
        2GKPPdIX/WIw3yiscaqn+7Dwut9FCDXz8WJ+JFW/aY8OuNBfjoPOZNHU6wXgKcYw
        1wGgDmRvyj2/MBGf7TdvO8XlTx7gVBCsBZnwmpk4XgGl4ugYykQkbZWwErlU5q6d
        6FYaks3I/id+R13Eqqxof8ZoTxP47M1OswAwi7LdUv844ootBV8oFyKG0sQUm999
        i2hNtVWNO1IT3iYerwIGJAsCAwEAAQ==
        -----END PUBLIC KEY-----`,
        metodo: 'encriptacionClaveA'
      },
      esCliente: true,
    }
  }
};
